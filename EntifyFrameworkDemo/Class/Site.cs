﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace EntifyFrameworkDemo.Class
{
    public class Site
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<MeasurementSystem> MeasurementSystems { get; set; }
    }

}
