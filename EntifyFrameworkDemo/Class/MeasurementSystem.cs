﻿using System;
using System.Collections.Generic;
using System.Net;

namespace EntifyFrameworkDemo.Class
{
    public class MeasurementSystem
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        public string HostName { get; set; }
        public Guid SiteId { get; set; }
        public virtual Site Site { get; set; }
    }
}
