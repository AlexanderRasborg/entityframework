namespace EntifyFrameworkDemo.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using EntifyFrameworkDemo.Class;

    internal sealed class Configuration : DbMigrationsConfiguration<EntifyFrameworkDemo.ProjectDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(EntifyFrameworkDemo.ProjectDbContext context)
        {
            context.Site.AddOrUpdate(x => x.Id,
            new Site() { Id = Guid.Parse("FFA0185A-53A4-4319-991A-F272A013ACEF"), Name = "Drantum1" });

            context.MeasurementSystems.AddOrUpdate(x => x.Id, 
                new MeasurementSystem() { Id = Guid.NewGuid(), HostName = "1.1.1.1", Name = "Nac", SiteId = Guid.Parse("FFA0185A-53A4-4319-991A-F272A013ACEF") });
        }
    }
}
