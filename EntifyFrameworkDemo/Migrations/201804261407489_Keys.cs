namespace EntifyFrameworkDemo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Keys : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MeasurementSystems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        HostName = c.String(),
                        SiteId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sites", t => t.SiteId, cascadeDelete: true)
                .Index(t => t.SiteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MeasurementSystems", "SiteId", "dbo.Sites");
            DropIndex("dbo.MeasurementSystems", new[] { "SiteId" });
            DropTable("dbo.MeasurementSystems");
        }
    }
}
