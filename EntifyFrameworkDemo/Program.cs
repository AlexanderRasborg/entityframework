﻿using System;
using System.Linq;
using EntifyFrameworkDemo.Class;

namespace EntifyFrameworkDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new ProjectDbContext())
            {
                // Create and save a new Blog 
                Console.Write("Enter a name for a new Site: ");
                var name = Console.ReadLine();

                var site = new Site { Name = name, Id = Guid.NewGuid() };
                db.Site.Add(site);
                db.SaveChanges();

                // Display all Blogs from the database 
                var query = from b in db.Site
                            orderby b.Name
                            select b;

                Console.WriteLine("All Sites in the database:");
                foreach (var item in query)
                {
                    Console.WriteLine(item.Name);
                }

                Console.Write("Enter a name for a new Measurement system: ");
                var systemName = Console.ReadLine();
                Console.Write("Enter a ip for the new Measurement system: ");
                var ip = Console.ReadLine();

                var measurementsystem = new MeasurementSystem { Name = systemName, Id = Guid.NewGuid(), HostName = ip };
                db.MeasurementSystems.Add(measurementsystem);
                db.SaveChanges();


                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }

        }
    }

    
}
