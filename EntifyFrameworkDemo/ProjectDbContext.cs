﻿using EntifyFrameworkDemo.Class;
using System.Data.Entity;

namespace EntifyFrameworkDemo
{
    class ProjectDbContext :DbContext
    {
        public DbSet<Site> Site { get; set; }
        public DbSet<MeasurementSystem> MeasurementSystems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            var mb = modelBuilder;
            mb.Entity<Site>()
                .HasKey(s => s.Id)
                .HasMany(s => s.MeasurementSystems);
            mb.Entity<MeasurementSystem>()
                .HasKey(ms => ms.Id)
                .HasRequired(ms => ms.Site);
        }
    }    
}
